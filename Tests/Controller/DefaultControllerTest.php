<?php
 
namespace Knp\KnoodleBundle\Tests\Controller;
 
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
 
class DefaultControllerTest extends WebTestCase
{

	public function testSubmitAnEmptyAnswerForm()
	{
	    $client = static::createClient();
	 
	    $crawler = $client->request('GET', '/knoodle/');
	 
	    $link = $crawler->selectLink('The survey number 4')->link();
	 
	    $crawler = $client->click($link);
	 
	    $link = $crawler->selectLink('Answer this survey')->link();
	 
	    $crawler = $client->click($link);
	 
	    $form = $crawler->selectButton('Validate')->form();
	 
	    $crawler = $client->submit($form);
	 
	    $this->assertTrue(
	        $crawler->filter('h3:contains("New Answer")')->count() > 0,
	        'The user is still in the answer new page'
	    );
	}

}