<?php
namespace Knp\KnoodleBundle\Tests\Entity;

use Knp\KnoodleBundle\Entity\Question;
class QuestionTest extends \PHPUnit_Framework_TestCase
{
	public function testGetChoices()
	{
	    $question = new Question();
	    $question->setFirstChoice('Red');
	    $question->setSecondChoice('Green');
	    $question->setThirdChoice('Blue');
	 
	    $this->assertEquals(
	        array(
	            1 => 'Red',
	            2 => 'Green',
	            3 => 'Blue'
	        ),
	        $question->getChoices(),
	        '->getChoices() returns an array of the available choices'
	    );
	}
}