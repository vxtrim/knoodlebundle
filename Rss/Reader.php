<?php
namespace Knp\KnoodleBundle\Rss;
 
class Reader
{

    private $link;
 
    public function __construct($link)
    {
        $this->link = $link;
    }


    public function process()
    {
        // Getting an RSS feed into SimpleXML
        $rawFeed = file_get_contents($this->link);
        $xml = new \SimpleXmlElement($rawFeed);
 
        // Getting at the individual articles
        $posts = array();
        foreach ($xml->channel->item as $item) {
            $posts[] = array(
                'title' => $item->title,
                'link'  => $item->link
            );
        }
 
        return $posts;
    }
}