Feature: survey search
	In order to be able to find specific survey
	As a visitor
	I need to be able to use search

 Scenario:
	Given I am on "/knoodle"
	When I fill in "q" with "survey number 1"
	And I press "search"
	Then I should see "survey number 1"