<?php
 
namespace Knp\KnoodleBundle\Controller;

use Knp\KnoodleBundle\Entity\Answer;

use Knp\KnoodleBundle\Form\Model\SurveyAnswers;
use Knp\KnoodleBundle\Form\Type\SurveyAnswersType;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response; 
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class DefaultController extends Controller
{

    private function findSurveyOr404($id)
    {
        $survey = $this
            ->getDoctrine()
            ->getRepository('KnpKnoodleBundle:Survey')
            ->find($id);

        if ( !$survey) {
            throw $this->createNotFoundException(sprintf(
                'The survey with id "%s" was not found.', $id
            ));
        }

        return $survey;
    }

	private function findSurveyQuestions($surveyId)
	{
	    return $this
	        ->getDoctrine()
	        ->getRepository('KnpKnoodleBundle:Question')
	        ->findBySurvey($surveyId);
	}

	/**
	 * @Template("KnpKnoodleBundle:Default:surveyList.html.twig")
	 */
    public function surveyListAction()
	{
	    $surveys = $this
	        ->getDoctrine()
	        ->getRepository('KnpKnoodleBundle:Survey')
	        ->findAllOrderByDate();
	 
	    return array('surveys' => $surveys);
	}

	public function surveyListByPopularityAction(Request $request)
	{
	    $surveys = $this
	        ->getDoctrine()
	        ->getRepository('KnpKnoodleBundle:Survey')
	        ->findAllOrderByPopularity();
	 
	    $template = sprintf(
	        'KnpKnoodleBundle:Default:surveyListByPopularity.%s.twig',
	        $request->getRequestFormat()
	    );
	 
	    return $this->render($template, array(
	        'surveys' => $surveys
	    ));
	}

	public function lastSurveysAction($limit = 5)
	{
	    $surveys = $this
	        ->getDoctrine()
	        ->getRepository('KnpKnoodleBundle:Survey')
	        ->findLatest($limit)
	    ;
	 
	    return $this->render('KnpKnoodleBundle:Default:lastSurveys.html.twig', array(
	        'surveys' => $surveys
	    ));
	}

	public function surveyShowAction($id)
	{
	    $survey = $this->findSurveyOr404($id);
	 
	    return $this->render('KnpKnoodleBundle:Default:surveyShow.html.twig', array(
	        'survey' => $survey
	    ));
	}

	public function answerNewAction($surveyId)
	{
		if (!$this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
	        throw new AccessDeniedException('You are not allowed to answer this survey.');
	    }
	    $survey = $this->findSurveyOr404($surveyId);
	    $form = $this->createForm(
	        new SurveyAnswersType(),
	        new SurveyAnswers($survey)
	    );
	 
	    return $this->render('KnpKnoodleBundle:Default:answerNew.html.twig', array(
	        'survey' => $survey,
	        'form' => $form->createView()
	    ));
	}

	public function answerCreateAction($surveyId, Request $request)
	{
	    $survey = $this->findSurveyOr404($surveyId);
	    $answers = new SurveyAnswers($survey);
	    $form    = $this->createForm(new SurveyAnswersType(), $answers);
	    $em      = $this->getDoctrine()->getEntityManager();
	 
	    $form->bind($request);
	 
	    if ($form->isValid()) {
	        foreach ($survey->getQuestions() as $question) {
	            $answer = new Answer();
	            $answer->setQuestion($question);
	            $answer->setAuthorFirstname($answers->firstname);
	            $answer->setAuthorLastname($answers->lastname);
	            $answer->setAuthorEmail($answers->email);
	            $answer->setChoice($answers->choices[$question->getId()]);
	 
	            $em->persist($answer);
	        }
	 
	        $em->flush();
	 
	        $this->get('session')->getFlashBag()->set('success', 'Thank you for responding!');
	 
	        return $this->redirect($this->generateUrl('knoodle_survey', array('id' => $surveyId)));
	    }
	 
	    return $this->render('KnpKnoodleBundle:Default:answerNew.html.twig', array(
	        'survey'    => $survey,
	        'form'      => $form->createView()
	    ));
	}

	public function surveySearchAction(Request $request)
	{
	    $q = $request->query->get('q');
	 
	    $surveys = $this
	        ->getDoctrine()
	        ->getRepository('KnpKnoodleBundle:Survey')
	        ->search($q)
	    ;
	 
	    return $this->render('KnpKnoodleBundle:Default:surveySearch.html.twig', array(
	        'q'       => $q,
	        'surveys' => $surveys
	    ));
	}

	 public function rssReaderAction()
	{
	    $reader = $this->container->get('knp_knoodle.rss');
	 
	    return $this->render('KnpKnoodleBundle:Default:rssReader.html.twig', array(
	        'posts' => $reader->process()
	    ));
	}
}