<?php
 
namespace Knp\KnoodleBundle\Entity;
 
use Doctrine\ORM\EntityRepository;
 
class SurveyRepository extends EntityRepository
{

	public function findAllOrderByDate()
    {
        return $this
            ->_em
            ->createQuery('
                SELECT s FROM KnpKnoodleBundle:Survey s
                ORDER BY s.createdAt DESC
            ')
            ->execute();
    }

     public function findAllOrderByPopularity()
	{
	    $dql = 'SELECT s, COUNT(a.id) num_answers';
	    $dql .= ' FROM KnpKnoodleBundle:Survey s';
	    $dql .= ' LEFT JOIN s.questions q';
	    $dql .= ' LEFT JOIN q.answers a';
	    $dql .= ' GROUP BY s';
	    $dql .= ' ORDER BY num_answers DESC';
	 
	    $results = $this->_em->createQuery($dql)->execute();
	 
	    return array_map(
	        function ($result) { return $result[0]; },
	        $results
	    );
	}

	public function findLatest($limit)
	{
	    return $this
	        ->createQueryBuilder('s')
	        ->orderBy('s.createdAt', 'DESC')
	        ->getQuery()
	        ->setMaxResults($limit)
	        ->execute()
	    ;
	}

	public function search($q)
	{
	    return $this
	        ->createQueryBuilder('s')
	        ->leftJoin('s.questions', 'q')
	        ->where('s.name LIKE :query')
	        ->setParameter('query', sprintf('%%%s%%', $q))
	        ->getQuery()
	        ->execute();
	}
}