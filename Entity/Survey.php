<?php

namespace Knp\KnoodleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Knp\KnoodleBundle\Entity\SurveyRepository")
 * @ORM\Table(name="survey")
 */
class Survey
{
    /** @ORM\Id @ORM\GeneratedValue @ORM\Column(type="integer") */
    private $id;
    /** @ORM\Column(type="string") */
    private $name;
    /** @ORM\Column(type="string", name="author_firstname") */
    private $authorFirstname;
    /** @ORM\Column(type="string", name="author_lastname") */
    private $authorLastname;
    /** @ORM\Column(type="string", name="author_email") */
    private $authorEmail;
    /** @ORM\Column(type="datetime", name="created_at") */
    private $createdAt;
    /** @ORM\OneToMany(targetEntity="Question", mappedBy="survey") */
    private $questions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Survey
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set authorFirstname
     *
     * @param string $authorFirstname
     * @return Survey
     */
    public function setAuthorFirstname($authorFirstname)
    {
        $this->authorFirstname = $authorFirstname;
    
        return $this;
    }

    /**
     * Get authorFirstname
     *
     * @return string 
     */
    public function getAuthorFirstname()
    {
        return $this->authorFirstname;
    }

    /**
     * Set authorLastname
     *
     * @param string $authorLastname
     * @return Survey
     */
    public function setAuthorLastname($authorLastname)
    {
        $this->authorLastname = $authorLastname;
    
        return $this;
    }

    /**
     * Get authorLastname
     *
     * @return string 
     */
    public function getAuthorLastname()
    {
        return $this->authorLastname;
    }

    /**
     * Set authorEmail
     *
     * @param string $authorEmail
     * @return Survey
     */
    public function setAuthorEmail($authorEmail)
    {
        $this->authorEmail = $authorEmail;
    
        return $this;
    }

    /**
     * Get authorEmail
     *
     * @return string 
     */
    public function getAuthorEmail()
    {
        return $this->authorEmail;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Survey
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Add questions
     *
     * @param Knp\KnoodleBundle\Entity\Question $questions
     * @return Survey
     */
    public function addQuestion(\Knp\KnoodleBundle\Entity\Question $question)
    {
        $this->questions[] = $question;
        $question->setSurvey($this);
    }

    /**
     * Remove questions
     *
     * @param Knp\KnoodleBundle\Entity\Question $questions
     */
    public function removeQuestion(\Knp\KnoodleBundle\Entity\Question $questions)
    {
        $this->questions->removeElement($questions);
    }

    /**
     * Get questions
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    public function __toString(){
        return (string)$this->name;
    }

}