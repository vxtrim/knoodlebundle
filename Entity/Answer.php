<?php
 
namespace Knp\KnoodleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** @ORM\Entity @ORM\Table(name="answer") */
class Answer
{
    /** @ORM\Id @ORM\GeneratedValue @ORM\Column(type="integer") */
    private $id;
    /** @ORM\ManyToOne(targetEntity="Question") */
    private $question;
    /** @ORM\Column(type="string", name="author_firstname") */
    private $authorFirstname;
    /** @ORM\Column(type="string", name="author_lastname") */
    private $authorLastname;
    /** @ORM\Column(type="string", name="author_email") */
    private $authorEmail;
    /** @ORM\Column(type="integer") */
    private $choice;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set authorFirstname
     *
     * @param string $authorFirstname
     * @return Answer
     */
    public function setAuthorFirstname($authorFirstname)
    {
        $this->authorFirstname = $authorFirstname;
    
        return $this;
    }

    /**
     * Get authorFirstname
     *
     * @return string 
     */
    public function getAuthorFirstname()
    {
        return $this->authorFirstname;
    }

    /**
     * Set authorLastname
     *
     * @param string $authorLastname
     * @return Answer
     */
    public function setAuthorLastname($authorLastname)
    {
        $this->authorLastname = $authorLastname;
    
        return $this;
    }

    /**
     * Get authorLastname
     *
     * @return string 
     */
    public function getAuthorLastname()
    {
        return $this->authorLastname;
    }

    /**
     * Set authorEmail
     *
     * @param string $authorEmail
     * @return Answer
     */
    public function setAuthorEmail($authorEmail)
    {
        $this->authorEmail = $authorEmail;
    
        return $this;
    }

    /**
     * Get authorEmail
     *
     * @return string 
     */
    public function getAuthorEmail()
    {
        return $this->authorEmail;
    }

    /**
     * Set choice
     *
     * @param integer $choice
     * @return Answer
     */
    public function setChoice($choice)
    {
        $this->choice = $choice;
    
        return $this;
    }

    /**
     * Get choice
     *
     * @return integer 
     */
    public function getChoice()
    {
        return $this->choice;
    }

    /**
     * Set question
     *
     * @param Knp\KnoodleBundle\Entity\Question $question
     * @return Answer
     */
    public function setQuestion(\Knp\KnoodleBundle\Entity\Question $question = null)
    {
        $this->question = $question;
    
        return $this;
    }

    /**
     * Get question
     *
     * @return Knp\KnoodleBundle\Entity\Question 
     */
    public function getQuestion()
    {
        return $this->question;
    }
}