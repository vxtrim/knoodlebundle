<?php
 
namespace Knp\KnoodleBundle\Entity;
 
use Doctrine\ORM\Mapping as ORM;
 
/** @ORM\Entity @ORM\Table(name="question") */
class Question
{
    /** @ORM\Id @ORM\GeneratedValue @ORM\Column(type="integer") */
    private $id;
    /** @ORM\ManyToOne(targetEntity="Survey") */
    private $survey;
    /** @ORM\Column(type="string") */
    private $sentence;
    /** @ORM\Column(type="string", name="first_choice") */
    private $firstChoice;
    /** @ORM\Column(type="string", name="second_choice") */
    private $secondChoice;
    /** @ORM\Column(type="string", name="third_choice") */
    private $thirdChoice;
    /** @ORM\OneToMany(targetEntity="Answer", mappedBy="question") */
    private $answers;  

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sentence
     *
     * @param string $sentence
     * @return Question
     */
    public function setSentence($sentence)
    {
        $this->sentence = $sentence;
    
        return $this;
    }

    /**
     * Get sentence
     *
     * @return string 
     */
    public function getSentence()
    {
        return $this->sentence;
    }

    /**
     * Set firstChoice
     *
     * @param string $firstChoice
     * @return Question
     */
    public function setFirstChoice($firstChoice)
    {
        $this->firstChoice = $firstChoice;
    
        return $this;
    }

    /**
     * Get firstChoice
     *
     * @return string 
     */
    public function getFirstChoice()
    {
        return $this->firstChoice;
    }

    /**
     * Set secondChoice
     *
     * @param string $secondChoice
     * @return Question
     */
    public function setSecondChoice($secondChoice)
    {
        $this->secondChoice = $secondChoice;
    
        return $this;
    }

    /**
     * Get secondChoice
     *
     * @return string 
     */
    public function getSecondChoice()
    {
        return $this->secondChoice;
    }

    /**
     * Set thirdChoice
     *
     * @param string $thirdChoice
     * @return Question
     */
    public function setThirdChoice($thirdChoice)
    {
        $this->thirdChoice = $thirdChoice;
    
        return $this;
    }

    /**
     * Get thirdChoice
     *
     * @return string 
     */
    public function getThirdChoice()
    {
        return $this->thirdChoice;
    }

    /**
     * Set survey
     *
     * @param Knp\KnoodleBundle\Entity\Survey $survey
     * @return Question
     */
    public function setSurvey(\Knp\KnoodleBundle\Entity\Survey $survey = null)
    {
        $this->survey = $survey;
    
        return $this;
    }

    /**
     * Get survey
     *
     * @return Knp\KnoodleBundle\Entity\Survey 
     */
    public function getSurvey()
    {
        return $this->survey;
    }

    /**
     * Add answers
     *
     * @param Knp\KnoodleBundle\Entity\Answer $answers
     * @return Question
     */
    public function addAnswer(\Knp\KnoodleBundle\Entity\Answer $answers)
    {
        $this->answers[] = $answers;
    
        return $this;
    }

    /**
     * Remove answers
     *
     * @param Knp\KnoodleBundle\Entity\Answer $answers
     */
    public function removeAnswer(\Knp\KnoodleBundle\Entity\Answer $answers)
    {
        $this->answers->removeElement($answers);
    }

    /**
     * Get answers
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    public function getChoices()
    {
        return array(
            1 => $this->getFirstChoice(),
            2 => $this->getSecondChoice(),
            3 => $this->getThirdChoice()
        );
    }

    public function getNumChoiceAnswers($choice)
    {
        $count = 0;
        foreach ($this->answers as $answer) {
            if ($choice === $answer->getChoice()) {
                $count++;
            }
        }
     
        return $count;
    }

     public function getNumFirstChoiceAnswers()
    {
        return $this->getNumChoiceAnswers(1);
    }
     
    public function getNumSecondChoiceAnswers()
    {
        return $this->getNumChoiceAnswers(2);
    }
     
    public function getNumThirdChoiceAnswers()
    {
        return $this->getNumChoiceAnswers(3);
    }

    public function __toString(){
        return (string)$this->survey;
    }
}