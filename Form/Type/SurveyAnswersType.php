<?php
namespace Knp\KnoodleBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SurveyAnswersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', 'text')
            ->add('lastname', 'text')
            ->add('email', 'email')
            ->add('choices', 'form')
        ;

        $surveyAnswers = $builder->getData();
        $survey = $surveyAnswers->getSurvey();

        foreach ($survey->getQuestions() as $question) {
            $builder
                ->get('choices')
                ->add((string) $question->getId(), 'choice', array(
                'label'    => $question->getSentence(),
                'choices'  => $question->getChoices(),
                'expanded' => true
            ));
        }
    }

    public function getName()
    {
        return 'survey_answers';
    }
}