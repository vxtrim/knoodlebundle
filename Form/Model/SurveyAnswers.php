<?php
namespace Knp\KnoodleBundle\Form\Model;

use Knp\KnoodleBundle\Entity\Survey;
use Symfony\Component\Validator\Constraints as Assert;

class SurveyAnswers
{
    /**
     * @Assert\NotBlank
     * @Assert\MaxLength(255)
     */
    public $firstname;

    /**
     * @Assert\NotBlank
     * @Assert\MaxLength(255)
     */
    public $lastname;

    /**
     * @Assert\NotBlank
     * @Assert\Email
     * @Assert\MaxLength(255)
     */
    public $email;

    /**
     * @Assert\All({
     *      @Assert\NotBlank,
     *      @Assert\Choice({1, 2, 3})
     * })
     */
    public $choices;

    private $survey;

    public function __construct(Survey $survey)
    {
        $this->survey = $survey;
    }
    public function getSurvey()
    {
        return $this->survey;
    }
}