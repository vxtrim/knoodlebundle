<?php
 
namespace Knp\KnoodleBundle\DataFixtures\ORM;
 
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Knp\KnoodleBundle\Entity\Survey;
use Knp\KnoodleBundle\Entity\Question;
use Knp\KnoodleBundle\Entity\Answer;
 
class Surveys extends AbstractFixture
{
    public function load(ObjectManager $manager)
    {
        $authors = array(
            array('Sheldon', 'Cooper', 's.cooper@knplabs.com'),
            array('Troy', 'Hanson', 't.hanson@knplabs.com'),
            array('Jasmine', 'Jill', 'j.jill@knplabs.com'),
            array('Raphael', 'Young', 'r.young@knplabs.com'),
            array('Kellie', 'Hughes', 'k.hughes@knplabs.com'),
            array('Oscar', 'Cruz', 'o.cruz@knplabs.com'),
            array('Hector', 'Riddle', 'h.riddle@knplabs.com'),
            array('Gary', 'Joseph', 'g.joseph@knplabs.com'),
            array('Amanda', 'Luna', 'a.luna@knplabs.com'),
            array('David', 'Hines', 'd.hines@knplabs.com'),
        );
 
        for ($i = 0; $i < 10; $i++) {
            $survey = new Survey();
            $survey->setName(sprintf('The survey number %d', $i + 1));
            $survey->setAuthorFirstname($authors[$i][0]);
            $survey->setAuthorLastname($authors[$i][1]);
            $survey->setAuthorEmail($authors[$i][2]);
            $survey->setCreatedAt(new \DateTime(sprintf('- %d minutes', (11 - $i) * 129)));
 
            for ($j = 0; $j < 5; $j++) {
                $question = new Question();
                $question->setSentence(sprintf('The question number %d', $j + 1));
                $question->setFirstChoice('The first choice');
                $question->setSecondChoice('The second choice');
                $question->setThirdChoice('The third choice');
 
                $survey->addQuestion($question);
 
                for ($k = 0; $k < 10 - $i; $k++) {
                    $answer = new Answer();
                    $answer->setAuthorFirstname($authors[$k][0]);
                    $answer->setAuthorLastname($authors[$k][1]);
                    $answer->setAuthorEmail($authors[$k][2]);
                    $answer->setChoice($k % 3 + 1);
 
                    $question->addAnswer($answer);
 
                    $manager->persist($answer);
                }
 
                $manager->persist($question);
            }
 
            $manager->persist($survey);
            $manager->flush();
        }
    }
}