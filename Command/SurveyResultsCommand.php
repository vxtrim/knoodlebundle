<?php

namespace Knp\KnoodleBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SurveyResultsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('knoodle:survey:results')
            ->setDescription('Displays a survey\'s results')
            ->addArgument(
            'id',
            InputArgument::REQUIRED,
            'The survey\'s id'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
	{
	    $container  = $this->getContainer();
	    $doctrine   = $container->get('doctrine');
	    $repository = $doctrine->getRepository('KnpKnoodleBundle:Survey');
	    $survey     = $repository->find($input->getArgument('id'));
	 
	    if (null === $survey) {
	        $output->writeln(sprintf(
			    '<error>The survey with id "%s" was not found.</error>',
			    $input->getArgument('id')
			));
	 
	        return 1;
	    }
	    $templating = $container->get('templating');
		$output->write($templating->render(
		    'KnpKnoodleBundle:Command:survey_results.cli.twig',
		    array(
		        'survey' => $survey
		    )
		));
	}
}