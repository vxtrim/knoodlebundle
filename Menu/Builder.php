<?php
namespace Knp\KnoodleBundle\Menu;
 
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
 
class Builder extends ContainerAware
{
	public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
 
        $menu->addChild('Surveys by date', array('route' => 'knoodle'));
        $menu->addChild('Surveys by popularity', array('route' => 'knoodle_popular'));
 		
 		 if ($this->container->get('security.context')->isGranted('ROLE_ADMIN')) {
		    $username = $user = $this->container->get('security.context')->getToken()->getUser()->getUsername();
		    $menu->addChild(sprintf('Logout (%s)', $username), array('route' => 'knoodle_logout'));
		} else {
		    $menu->addChild('Login', array('route' => 'knoodle_login'));
		}

        return $menu;
    }
}